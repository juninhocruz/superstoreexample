export const genres = [
  {'id': 10, 'name': 'Racing'}, {'id': 11, 'name': 'Real Time Strategy (RTS)'}, {'id': 12, 'name': 'Role-playing (RPG)'}, {'id': 13, 'name': 'Simulator'}, {'id': 14, 'name': 'Sport'}, {'id': 15, 'name': 'Strategy'}, {'id': 16, 'name': 'Turn-based strategy (TBS)'}, {'id': 24, 'name': 'Tactical'}, {'id': 25, 'name': 'Hack and slash/Beat \'em up'}, {'id': 26, 'name': 'Quiz/Trivia'}
];
