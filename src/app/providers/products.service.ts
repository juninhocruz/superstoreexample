import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { C } from '../constants/app.constants';

@Injectable()
export class ProductsService {
  public products: Array<any>;
  public genres: Array<any>;

  constructor(private http: HttpClient) {
    this.products = [];
    this.genres = [];
    this.getData();
  }

  public getData() {
    this.updateProducts();
    this.updateGenres();
  }

  private updateProducts() {
    this.http.get(
      C.API_SERVER + '/games/',
      {
        headers: new HttpHeaders().set('user-key', C.API_KEY),
        params: new HttpParams().set('fields', '*'),
        observe: 'response',
      }
    )
      .subscribe((response) => {
          const games = response.body;
          if (games instanceof Array) {
            games.forEach((game) => {
              this.products.push(game);
            });
          }
      },
        (error) => {
        console.log(error);
      });
  }

  private updateGenres() {
    this.http.get(
      C.API_SERVER + '/genres/',
      {
        headers: new HttpHeaders().set('user-key', C.API_KEY),
        params: new HttpParams().set('fields', '*'),
        observe: 'response',
      }
    )
      .subscribe((response) => {
          const genres = response.body;
          if (genres instanceof Array) {
            genres.forEach((genre) => {
              this.genres.push(genre);
            });
          }
        },
        (error) => {
          console.log(error);
        });
  }
}
