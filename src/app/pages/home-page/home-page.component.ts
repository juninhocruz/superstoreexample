import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../providers/products.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.sass']
})
export class HomePageComponent implements OnInit {

  constructor(public products: ProductsService) { }

  ngOnInit() {
  }
}
