import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { ProductsPageComponent } from './pages/products-page/products-page.component';
import { ProductPageComponent } from './pages/product-page/product-page.component';
import { Error404PageComponent } from './pages/error-404-page/error-404-page.component';
import { AboutPageComponent } from './pages/about-page/about-page.component';
import { CartPageComponent } from './pages/cart-page/cart-page.component';

const routes: Routes = [
  // home
  {
    path: '',
    component: HomePageComponent,
  },
  // products
  {
    path: 'products',
    component: ProductsPageComponent,
  },
  // product
  {
    path: 'products/:slug/:id',
    component: ProductPageComponent,
  },
  // cart
  {
    path: 'cart',
    component: CartPageComponent,
  },
  // about
  {
    path: 'about',
    component: AboutPageComponent,
  },
  // 404 - not found
  {
    path: '**',
    component: Error404PageComponent,
  },
];

export const RoutingModule =  RouterModule.forRoot(routes);
