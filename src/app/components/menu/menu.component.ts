import { Component, OnInit } from '@angular/core';
import { MenuService } from '../../providers/menu.service';
import { ProductsService } from '../../providers/products.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.sass']
})
export class MenuComponent implements OnInit {

  constructor(public menu: MenuService, public products: ProductsService) { }

  ngOnInit() {
  }
}
