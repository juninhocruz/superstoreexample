import { Injectable } from '@angular/core';
import { ProductsService } from './products.service';
import { general } from '../data/general.menu';
import { genres } from '../data/genres.menu';

@Injectable()
export class MenuService {
  public isOpened: boolean;
  public isClicked: boolean;

  public options: Array<any> = [
    {
      name: 'general',
      values: general,
      icon: '/assets/ic_home.svg',
    },
    {
      name: 'genres',
      values: this.products.genres,
      icon: '/assets/ic_category.svg',
    },
  ];

  constructor(private products: ProductsService) {
  }

  open() {
    this.isOpened = true;
    this.isClicked = true;
  }

  close() {
    this.isOpened = false;
    this.isClicked = true;
  }
}
